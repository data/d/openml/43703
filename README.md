# OpenML dataset: Pantheon-Project-Historical-Popularity-Index

https://www.openml.org/d/43703

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Pantheon is a project celebrating the cultural information that endows our species with these fantastic capacities. To celebrate our global cultural heritage we are compiling, analyzing and visualizing datasets that can help us understand the process of global cultural development. Dive in, visualize, and enjoy.
Content
The Pantheon 1.0 data measures the global popularity of historical characters using two measures. The simpler of the two measures, which we denote as L, is the number of different Wikipedia language editions that have an article about a historical character. The more sophisticated measure, which we name the Historical Popularity Index (HPI) corrects L by adding information on the age of the historical character, the concentration of page views among different languages, the coefficient of variation in page views, and the number of page views in languages other than English.
For annotations of specific values visit the column metadata in the /Data tab. A more comprehensive breakdown is available on the Parthenon website.
Acknowledgements
Pantheon is a project developed by the Macro Connections group at the Massachusetts Institute of Technology Media Lab. For more on the dataset and to see visualizations using it, visit its landing page on the MIT website.
Inspiration
Which historical figures have a biography in the most languages? Who received the most Wikipedia page views? Which occupations or industries are the most popular? What country has the most individuals with a historical popularity index over twenty?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43703) of an [OpenML dataset](https://www.openml.org/d/43703). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43703/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43703/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43703/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

